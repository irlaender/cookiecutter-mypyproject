#!/usr/bin/env python

"""Tests for `{{cookiecutter.project_slug}}` package."""

from click.testing import CliRunner

from {{cookiecutter.project_slug}} import cli


def test_command_line_interface():
    """Test the CLI."""
    runner = CliRunner()
    result = runner.invoke(cli.main)
    assert result.exit_code == 0
    help_result = runner.invoke(cli.main, ["--help"])
    assert help_result.exit_code == 0
    assert "--help  Show this message and exit." in help_result.output
