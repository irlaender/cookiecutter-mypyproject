# Cookiecutter template for python projects

## Example

Checkout this [this example
project](https://gitlab.com/irlaender/mypyproject) to see how a project
looks like after initialisation.

## Usage

Install cookiecutter:

```bash
pip install cookiecutter
```

Try it out with default values, which can be modified during initialisation:

```bash
cookiecutter https://gitlab.com/irlaender/cookiecutter-mypyproject
```

If you use this template often then you can use your default values
during creation. See [User
Config](https://cookiecutter.readthedocs.io/en/1.7.3/advanced/user_config.html)
for more details. Create a file named `my-custom-config.yaml` with the
follwing content:

```yaml
default_context:
  author_name: "Your Name"
  author_email: "your.mail@example.com"
  docker_username: "login@docker.io"
```

Then call cookiecutter like this:

```bash
cookiecutter --config-file https://gitlab.com/irlaender/cookiecutter-mypyproject
```

## Features

Use `make` to unify your tooling for for common tasks like testing,
formatting, installation, packaging.

* Docker: Build, run and publish your application.
* Testing: Use `pytest` for tests.
* Codestyle: Use `black` and `isort` to ensure unified codestyle.
* Linting: Use `mypy` for typing and `flake8` to find codesmells.
* Legal: Use `pip-license` to list all available licenses in this package.
