## v0.5.0 (2023-09-29)

### Feat

- Added debugy as dev requirement
- Added logger
- Support for .env file. File will be loaded on startup

### Fix

- Added missing material theme to build docs
- Upgrade pip to latest version on make develop
- coverage.xml not ignored in git

### Refactor

- Add requirements line by line to improve readability and diffing on updates

## v0.4.0 (2023-07-17)

### Feat

- Added default comittizen config
- Added default sonarcloud config

### Fix

- Initialize empty git repository if it does not already exist

## v0.3.0 (2023-07-17)

### Feat

- Docker build is based on requirements.txt
- Added mkdocs

### Fix

- Use semantical version 0.1.0

## v0.2.0 (2023-07-17)

### Feat

- Added commitizen
- Added pre-commit hook.
- **docker**: Added automated tagging for docker images.
- Improved Lint/Formatting, Added xml report for coverage.

### Fix

- **docker**: Fixed building tags if no tags is provided.
- Changed order of linting steps.

## v0.1.0 (2023-07-17)

### Feat

- Show missing lines on coverage
- Added command for coverage.
- Added package root and meta data to package __init__
- Add linting using mypy and flake8
- Added support for docker.
- Added make check-license to makefile.
- Added AUTHORS file.
- Added MIT license
- Added tests
- Very basic package

### Fix

- Duplicated command in Makefile.
- **linting**: Added missing config.
- Wrong name of dependency.

## v0.0.1 (2023-07-17)
